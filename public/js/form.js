function Product(id, name, qty, price, createddate){
	this.id = ko.observable(id);
	this.name  =  ko.observable(name);
	this.qty  =  ko.observable(qty);
	this.price  =  ko.observable(price);
	this.createddate  =  ko.observable(createddate);

	var timestamp = Math.round(new Date(createddate).getTime()/1000);
	this.orderColumn = ko.observable(timestamp);

	//total value
	var total = qty*price;
	this.total = ko.observable(total);
}

function productViewModel(){
	this.products = ko.observableArray([]);
}

var viewModel  = new productViewModel();

function isInt(value) {
    return value.match(/^-?\d*(\.\d+)?$/);
}

$(document).ready(function(){
	ko.applyBindings(viewModel);

	$("#btnAddProduct").click(function(){
		//checking:
		if($("#productName").val().trim() == ""){
			alert("Product Name can't be empty!");
		}else if(isNaN($("#qty").val()) || $("#qty").val() == ""){
			alert("Qty must be a number!");
		}else if(isNaN($("#price").val()) || $("#price").val() == ""){
			alert("Price must be a number!");
		}else {
			$.post($("#addProductUrl").val(),
				{
					_token: $("#_token").val(),
					name: $("#productName").val(),
					qty: $("#qty").val(),
					price: $("#price").val()
				},
				function (data){
					if(data.status == "OK"){
						//display data:
						var product = new Product(data.uniqueid, $("#productName").val(), $("#qty").val(), $("#price").val(), data.createddate);
						viewModel.products.push(product);

						viewModel.products.sort((function(left, right) { return left.orderColumn() < right.orderColumn() ? 1 : -1; }));
					}else{
						alert(data.message);
					}
				}
			)
			.error(function (er) {
				console.log("Error:", er.responseText);
				alert('failed to add Product!');
			});
		}
	});
});