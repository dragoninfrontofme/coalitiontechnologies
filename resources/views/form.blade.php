<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Coalition</title>
    <link rel="icon" type="image/png" href="{{ URL::asset("assets/img/ico.png") }}" />

    <!-- Bootstrap -->
    <link href="{{ URL::asset("css/app.css") }}" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link href='//fonts.googleapis.com/css?family=Josefin+Sans:400,600|Playfair+Display:400,700italic' rel='stylesheet' type='text/css'>
    <link href='//fonts.googleapis.com/css?family=EB+Garamond' rel='stylesheet' type='text/css'>
    <link href="{{ URL::asset("css/app.css") }}" rel="stylesheet">
    <style>
      .defaultContainer {margin-top:80px;}
      .form-wrapper {width:450px;}
      .form-wrapper .form-control {margin:10px 0px;}
    </style>
  </head>
  <body>
    <input type="hidden" id="addProductUrl" value="{{ action('FormController@doSubmit') }}" />

    <nav class="navbar navbar-default navbar-fixed-top mainnav-bar">
      <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#menu">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="{{ action('FormController@index') }}">Coalition</a>
        </div>
      </div><!-- /.container-fluid -->
    </nav>
    
    <!-- Content here -->
    <div class="container defaultContainer">
      <div class="row">
        <div class="col-sm-12 title">
          Products Form:
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12">
          <div class="form-wrapper">
            <input type="hidden" id="_token" value="{{{ csrf_token() }}}" />
            <input type="text" class="form-control" id="productName" name="name" placeholder="Product Name">
            <input type="text" class="form-control" id="qty" name="name" placeholder="Quantity in stock">
            <input type="text" class="form-control" id="price" name="name" placeholder="Price">
            <button class="btn btn-primary" id="btnAddProduct">Submit</button>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12">
          <br></br>
          <table class="table table-striped">
            <thead>
              <tr>
                <th>
                  Tools
                </th>
                <th>
                  Product Name
                </th>
                <th>
                  Quantity iun stock
                </th>
                <th>
                  Price per Item
                </th>
                <th>
                  Datetime Submitted
                </th>
                <th>
                  Total Value Number
                </th>
              </tr>
            </thead>

            <tbody data-bind="template: {name: 'product-template', foreach: products}">
            </tbody>
          </table>
        </div>
      </div>
    </div>

    <script type="text/html" id="product-template">
      <tr>
        <td>
          <span>Edit</span>
        </td>
        <td>
          <span data-bind="text: name"></span>
        </td>
        <td>
          <span data-bind="text: qty"></span>
        </td>
        <td>
          <span data-bind="text: price"></span>
        </td>
        <td>
          <span data-bind="text: createddate"></span>
        </td>
        <td>
          <span data-bind="text: total"></span>
        </td>
      </tr>
    </script>


    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="{{ URL::asset("js/jquery.min.js") }}"></script>
    <script src="{{ URL::asset("js/external/knockout-3.3.0.js") }}"></script>
    <script src="{{ URL::asset("js/form.js") }}"></script>
  </body>
</html>