<?php namespace App\Http\Controllers;

use Auth;
use Session;
use Request;
use Response;
use File;

class FormController extends Controller {

	public function __construct()
	{ 
		//Middleware here
	}

	public function index(){
		//reset files:
		$file = "products.txt";
		File::put(storage_path($file), "");

		return view('form');
	}

	public function doSubmit(){
		if(Request::ajax())
		{
			$name = Request::input('name');
			$qty = Request::input('qty');
			$price = Request::input('price');

			//generate uniqueid:
			$uniqid = md5(uniqid(rand(), true));
			$date = new \DateTime();

			$file = "products.txt";

			//TODO: checker:

			$json = json_decode(File::get(storage_path($file)));

			if($json == ""){
				$json = array(array("id" => $uniqid, "productname" => $name, "qty" => $qty, "price" => $price, "createddate" => $date->format('Y-m-d H:i:s')));
			}else{
				$index = sizeof($json);
				$json[$index] = array("id" => $uniqid, "productname" => $name, "qty" => $qty, "price" => $price, "createddate" => $date->format('Y-m-d H:i:s'));
			}

			//file_put_contents(storage_path($file), json_encode($json));
			File::put(storage_path($file), json_encode($json));

			return Response::json(array('status' => 'OK', 'message' => 'Success', 'uniqueid' => $uniqid, "createddate" => $date->format('Y-m-d H:i:s')));
		}
		
		return Response::json(array('status' => 'ERROR', 'message' => 'is not valid ajax request!'));
	}
}